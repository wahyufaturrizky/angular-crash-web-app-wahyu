# Bank Mandiri Web by Wahyu Fatur Rizki

Bank Mandiri Web for employee management including update, create and delete also data tables.
DEMO APP: https://web-bank-mandiri-wahyu.netlify.app/
User: wahyufaturrizkyy@gmail.com
Pass: Welcome1
Gitlab: https://gitlab.com/wahyufaturrizky/angular-crash-web-app-wahyu

Author: [Wahyu Fatur Rizky](https://www.linkedin.com/in/wahyu-fatur-rizky)

## Table of Contents

- [Features](#features)
- [Technologies and Packages Used](#technologies-and-packages-used)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [Contact](#contact)

## Features

- Login Authentication
- Password Authentication
- SuppaBase Data Management
- Employee Management
- Create New Employee
- Update Employee
- Delete Employee
- Paginators
- Filter
- Sort Employee

## Technologies and Packages Used

This project is developed using the following technologies and packages:

- **AngularMaterialDesign**: The Angular team builds and maintains both common UI components and tools to help you build your own custom components. The team maintains several npm packages.
- **SupaBase**: Supabase is an open source Firebase alternative.
  Start your project with a Postgres database, Authentication, instant APIs, Edge Functions, Realtime subscriptions, Storage, and Vector embeddings.

## Installation

1. Clone the repository to your local machine using:

   ```
   git clone https://gitlab.com/wahyufaturrizky/angular-crash-web-app-wahyu.git
   ```

2. Navigate to the project directory:

   ```
   cd angular-crash-web-app-wahyu
   ```

3. Install the project dependencies using npm:

   ```
   npm install
   ```

## Usage

1. Start the development server:

   ```
   npm start
   ```

2. Open your web browser and navigate to `http://localhost:4200`.

3. Login with the username = "wahyufaturrizkyy@gmail.com" and password = "Welcome1"

## Contact

For any inquiries or feedback, feel free to connect with the author on [LinkedIn](https://www.linkedin.com/in/wahyu-fatur-rizky).

---

If you encounter any issues or have suggestions for improvement, please don't hesitate to reach out to us. Happy exploring!
