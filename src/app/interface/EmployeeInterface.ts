export interface ResEmployeeInterce {
  id: string;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}
