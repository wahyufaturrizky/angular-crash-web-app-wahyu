import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ResEmployeeInterce } from '@app/interface/EmployeeInterface';
import { ResUserInterce } from '@app/interface/UserInterface';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  @Input() dataTable: Array<ResEmployeeInterce> | undefined;

  @Output() onHandleView: EventEmitter<any> = new EventEmitter();
  @Output() onHandleEdit: EventEmitter<any> = new EventEmitter();
  @Output() onHandleDelete: EventEmitter<any> = new EventEmitter();

  constructor() {}

  handleView(data: ResEmployeeInterce): void {
    this.onHandleView.emit(data);
  }

  handleEdit(data: ResEmployeeInterce): void {
    this.onHandleEdit.emit(data);
  }

  handleDelete(data: ResEmployeeInterce): void {
    this.onHandleDelete.emit(data);
  }

  ngOnInit(): void {}
}
