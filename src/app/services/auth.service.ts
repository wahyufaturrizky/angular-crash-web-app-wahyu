import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  ReqLoginInterface,
  ReqRegisterInterface,
  ResUserInterce,
} from '@app/interface/UserInterface';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

import {
  AuthChangeEvent,
  AuthSession,
  createClient,
  Session,
  SupabaseClient,
  User,
} from '@supabase/supabase-js';

export interface Profile {
  id?: string;
  username: string;
  website: string;
  avatar_url: string;
}

const dataUser: string | null = localStorage.getItem('userData');
const dataUserParse = dataUser && JSON.parse(dataUser);

const httpHeader = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

const httpHeaderPrivate = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: `Bearer ${dataUserParse?.access_token}`,
  }),
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private supabase: SupabaseClient;
  _session: AuthSession | null = null;

  private apiUrl: string = environment.supabaseUrl;

  constructor(private http: HttpClient) {
    this.supabase = createClient(
      environment.supabaseUrl,
      environment.supabaseKey
    );
  }

  get session() {
    this.supabase.auth.getSession().then(({ data }) => {
      this._session = data.session;
    });
    return this._session;
  }

  profile(user: User) {
    return this.supabase
      .from('profiles')
      .select(`username, website, avatar_url`)
      .eq('id', user.id)
      .single();
  }

  authChanges(
    callback: (event: AuthChangeEvent, session: Session | null) => void
  ) {
    return this.supabase.auth.onAuthStateChange(callback);
  }

  signIn(email: string, password: string) {
    return this.supabase.auth.signInWithPassword({
      email,
      password,
    });
  }

  signOut() {
    return this.supabase.auth.signOut();
  }

  updateProfile(profile: Profile) {
    const update = {
      ...profile,
      updated_at: new Date(),
    };

    return this.supabase.from('profiles').upsert(update);
  }

  downLoadImage(path: string) {
    return this.supabase.storage.from('avatars').download(path);
  }

  uploadAvatar(filePath: string, file: File) {
    return this.supabase.storage.from('avatars').upload(filePath, file);
  }

  PostLogin(
    data: ReqLoginInterface | undefined
  ): Observable<ReqLoginInterface | undefined> {
    return this.http.post<ReqLoginInterface | undefined>(
      `${this.apiUrl}/auth/login`,
      data,
      httpHeader
    );
  }

  PostRegister(
    data: ReqRegisterInterface | undefined
  ): Observable<ReqRegisterInterface> {
    return this.http.post<ReqRegisterInterface>(
      `${this.apiUrl}/auth/register`,
      data,
      httpHeader
    );
  }

  PutUser(
    data: ReqRegisterInterface,
    id: string | undefined
  ): Observable<ReqRegisterInterface> {
    return this.http.patch<ReqRegisterInterface>(
      `${this.apiUrl}/api/users/${id}`,
      data,
      httpHeaderPrivate
    );
  }

  GetLitUsers(): Observable<Array<ResUserInterce>> {
    return this.http.get<Array<ResUserInterce>>(
      `${this.apiUrl}/api/users/`,
      httpHeaderPrivate
    );
  }

  DeleteUser(id: string | undefined): Observable<any> {
    return this.http.delete<any>(
      `${this.apiUrl}/api/users/${id}`,
      httpHeaderPrivate
    );
  }
}
