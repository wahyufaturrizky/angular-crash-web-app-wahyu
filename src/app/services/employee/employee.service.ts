import { Injectable } from '@angular/core';
import { ResEmployeeInterce } from '@app/interface/EmployeeInterface';
import { environment } from '@env/environment';
import { createClient, SupabaseClient } from '@supabase/supabase-js';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private supabase: SupabaseClient;

  constructor() {
    this.supabase = createClient(
      environment.supabaseUrl,
      environment.supabaseKey
    );
  }

  GetListEmployee() {
    return this.supabase.from('employee').select();
  }

  PostNewEmployee(employee: ResEmployeeInterce) {
    const payload = {
      ...employee,
      created_at: new Date(),
    };

    return this.supabase.from('employee').insert(payload).select();
  }

  UpdateEmployee(employee: ResEmployeeInterce, id: string) {
    const payload = {
      ...employee,
      updated_at: new Date(),
    };

    return this.supabase.from('employee').update(payload).eq('id', id).select();
  }

  DeleteEmployee(id: string) {
    return this.supabase.from('employee').delete().eq('id', id).select();
  }
}
