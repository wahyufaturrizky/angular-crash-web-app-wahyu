import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ResEmployeeInterce } from '@app/interface/EmployeeInterface';
import { ModalInterface } from '@app/interface/ModalInterface';
import { ResUserInterce } from '@app/interface/UserInterface';
import { AuthService } from '@app/services/auth.service';
import { EmployeeService } from '@app/services/employee/employee.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = [
    'username',
    'firstName',
    'lastName',
    'email',
    'action',
  ];

  constructor(
    private router: Router,
    private authService: AuthService,
    private employeeService: EmployeeService
  ) {}

  dataSource = new MatTableDataSource<ResEmployeeInterce>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  isLoading: boolean = false;
  isRateLimitReached: boolean = false;
  formData: ResEmployeeInterce | undefined;
  modal: ModalInterface = {
    isShow: false,
    headerTitle: '',
    message: '',
    dataRow: undefined,
    listMenu: [],
  };

  checkIsAuthenticated(): void {
    const res = localStorage.getItem('userData');
    res ? undefined : this.router.navigate(['/']);
  }

  onhandleCloseModal(data: boolean): void {
    this.modal = { ...this.modal, isShow: data };
  }

  async handleGetListEmployee(): Promise<void> {
    try {
      this.isLoading = true;

      const { error, data } = await this.employeeService.GetListEmployee();

      if (error) throw error;

      if (data) {
        this.dataSource = data as any;
        this.isLoading = false;
      }
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    } finally {
      this.isLoading = false;
    }
  }

  onHandleEdit(data: ResUserInterce): void {
    this.modal = {
      ...this.modal,
      isShow: true,
      dataRow: data,
      message: '',
      headerTitle: 'EDIT',
      listMenu: [
        {
          fieldLabel: 'Username',
          fieldName: 'username',
          fieldPlaceholder: 'Please fill username',
          fieldType: 'text',
          required: true,
          defaultValue: data.username,
        },
        {
          fieldLabel: 'First Name',
          fieldName: 'firstName',
          fieldPlaceholder: 'Please fill first name',
          fieldType: 'text',
          required: true,
          defaultValue: data.firstName,
        },
        {
          fieldLabel: 'Last Name',
          fieldName: 'lastName',
          fieldPlaceholder: 'Please fill last name',
          fieldType: 'text',
          required: true,
          defaultValue: data.lastName,
        },
        {
          fieldLabel: 'Email',
          fieldName: 'email',
          fieldPlaceholder: 'Please fill email',
          fieldType: 'email',
          required: true,
          defaultValue: data.email,
        },
        {
          fieldLabel: 'Phone Number',
          fieldName: 'phoneNumber',
          fieldPlaceholder: 'Please fill phone number',
          fieldType: 'number',
          required: true,
          defaultValue: data.phoneNumber,
          hidden: true,
        },
      ],
    };
  }

  handleAdd(): void {
    this.modal = {
      ...this.modal,
      isShow: true,
      headerTitle: 'ADD',
      listMenu: [
        {
          fieldLabel: 'Username',
          fieldName: 'username',
          fieldPlaceholder: 'Please fill username',
          fieldType: 'text',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Password',
          fieldName: 'password',
          fieldPlaceholder: 'Please fill password',
          fieldType: 'password',
          required: true,
          hidden: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'First Name',
          fieldName: 'firstName',
          fieldPlaceholder: 'Please fill first name',
          fieldType: 'text',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Last Name',
          fieldName: 'lastName',
          fieldPlaceholder: 'Please fill last name',
          fieldType: 'text',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Email',
          fieldName: 'email',
          fieldPlaceholder: 'Please fill email',
          fieldType: 'email',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Phone Number',
          fieldName: 'phoneNumber',
          fieldPlaceholder: 'Please fill phone number',
          fieldType: 'number',
          required: true,
          defaultValue: '',
          hidden: true,
        },
        {
          fieldLabel: 'Birth Date',
          fieldName: 'birthDate',
          fieldPlaceholder: 'Please fill birth date',
          fieldType: 'date',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Basic Salary',
          fieldName: 'basicSalary',
          fieldPlaceholder: 'Please fill basic Salary',
          fieldType: 'number',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Status',
          fieldName: 'status',
          fieldPlaceholder: 'Please fill status',
          fieldType: 'text',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Group',
          fieldName: 'group',
          fieldPlaceholder: 'Please fill group',
          fieldType: 'text',
          required: true,
          defaultValue: '',
        },
        {
          fieldLabel: 'Description',
          fieldName: 'description',
          fieldPlaceholder: 'Please fill description',
          fieldType: 'text',
          required: true,
          defaultValue: '',
        },
      ],
    };
  }

  handleUpdate(): void {
    this.isLoading = true;
    let data = {
      ...this.formData,
      isActive: false,
    };
    this.authService.PutUser(data, this.modal.dataRow?.id).subscribe(
      (data) => {
        this.isLoading = false;
        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Success Update',
          message: 'Success update data' + data.username,
        };
      },
      (err) => {
        this.isLoading = false;
        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Failed Update',
          message: err.error.message,
        };
      },
      () => this.handleGetListEmployee()
    );
  }

  onHandleChange(data: any): void {
    this.formData = {
      ...this.formData,
      [data.target.name]: data.target.value,
    } as ResEmployeeInterce;
  }

  handleAddData(): void {
    this.isLoading = true;
    let data = {
      ...this.formData,
      isActive: false,
    };
    this.authService.PostRegister(data).subscribe(
      (data) => {
        this.isLoading = false;
        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Success Update',
          message: 'Success update data' + data.username,
        };
      },
      (err) => {
        this.isLoading = false;
        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Failed Update',
          message: err.error.message,
        };
      },
      () => this.handleGetListEmployee()
    );
  }

  async onSubmitNewData(): Promise<void> {
    try {
      this.isLoading = true;

      const dataRaw = this.formData as ResEmployeeInterce;

      const { error, data } = await this.employeeService.PostNewEmployee(
        dataRaw
      );
      console.log('@data', data);

      if (error) throw error;

      if (data) {
        this.isLoading = false;

        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Success Add',
          message: 'Success add data',
        };
      }
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    } finally {
      this.handleGetListEmployee();
      this.isLoading = false;
    }
  }

  async onUpdatedData(): Promise<void> {
    try {
      this.isLoading = true;

      const dataRaw = this.formData as ResEmployeeInterce;

      const { error, data } = await this.employeeService.UpdateEmployee(
        dataRaw,
        this.modal.dataRow.id
      );

      if (error) throw error;

      if (data) {
        this.isLoading = false;

        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Success Update',
          message: 'Success update data',
        };
      }
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    } finally {
      this.handleGetListEmployee();
      this.isLoading = false;
    }
  }

  async onDeleteData(): Promise<void> {
    try {
      this.isLoading = true;

      const { error, data } = await this.employeeService.DeleteEmployee(
        this?.modal?.dataRow?.id
      );

      if (error) throw error;

      if (data) {
        this.isLoading = false;

        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Success Delete',
          message: 'Success delete data',
        };
      }
    } catch (error) {
      if (error instanceof Error) {
        alert(error.message);
      }
    } finally {
      this.handleGetListEmployee();
      this.isLoading = false;
    }
  }

  onHandleDelete(data: ResUserInterce): void {
    this.modal = {
      ...this.modal,
      isShow: true,
      headerTitle: 'DELETE',
      dataRow: data,
      message: 'Are you sure want to delete this data?',
    };
  }

  onHandleView(data: ResUserInterce): void {
    this.modal = {
      ...this.modal,
      isShow: true,
      headerTitle: 'VIEW',
      dataRow: data,
      message: '',
    };
  }

  onDelete(): void {
    this.isLoading = true;
    this.authService.DeleteUser(this.modal.dataRow?.id).subscribe(
      (data) => {
        this.isLoading = false;
        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Success Delete',
          message: 'Success delete this data',
        };
      },
      (err) => {
        this.isLoading = false;
        this.modal = {
          ...this.modal,
          isShow: true,
          headerTitle: 'Failed Delete',
          message: err.error.message,
        };
      },
      () => this.handleGetListEmployee()
    );
  }

  ngOnInit(): void {
    this.checkIsAuthenticated();
    this.handleGetListEmployee();
  }
}
